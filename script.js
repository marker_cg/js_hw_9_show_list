// 1
function listOnPage1(arr, parent) {
    let list = arr.map(element => {
        return `<li>${element}</li>`
    })
    
    let htmlList = list.join('');
    parent.insertAdjacentHTML('afterbegin', `<ul>${htmlList}</ul>`);
}


// 2
function listOnPage2(arr, parent) {
    let list = arr.map(element => {
        inList = '';

        if (Array.isArray(element)) {
            inList = element.map(element => {
                return `<li>${element}</li>`
            });

            let stringInList = inList.join('');
            return `<ul>${stringInList}</ul>`
            
        
        } else {
            return `<li>${element}</li>`
        } 
    })
    
    let stringList = list.join('');
    parent.insertAdjacentHTML('afterbegin', `<ul>${stringList}</ul>`);
}


// Output
listOnPage2(["USA",["New-York", "Chicago"], "Ukraine", ["Kiev", "Kharkiv", "Odessa", "Lviv"]], document.body)
listOnPage1(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], document.body);













